
FROM mcr.microsoft.com/powershell:alpine-3.10 AS powershell
ENTRYPOINT ["pwsh"]
#ARG POWER_VRO_VERSION

ARG POWER_VRA_VERSION
ARG POWER_VRO_VERSION


WORKDIR /modules

#RUN ls
RUN pwsh -command install-module powervra -force -requiredversion ${POWER_VRA_VERSION}
RUN pwsh -command install-module powervro -force -requiredversion ${POWER_VRO_VERSION}

#FROM $BASE
#
RUN apk add --no-cache jq
RUN apk add --no-cache curl


# Override ENTRYPOINT since hashicorp/terraform uses `terraform`
ENTRYPOINT []
#CMD ["pwsh"]
